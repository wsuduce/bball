﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Bball.viewModels;

namespace Bball.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Learn about your team!";

            string appName = "Your First ASP.NET 5 App";
            ViewBag.Message = "Your Application Name: " + appName;            

            var serverInfo = new ServerInfoViewModel()
            {
                Name = Environment.MachineName,
                Software = Environment.OSVersion.ToString()
            };
            return View(serverInfo);



           
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "How we can stay connected";

            return View();
        }

        public IActionResult Error()
        {
            return View("~/Views/Shared/Error.cshtml");
        }
    }
}
