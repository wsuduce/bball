﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bball.viewModels
{
    public class ServerInfoViewModel
    {
        public string Name { get; set; }
        public string LocalAddress { get; set; }
        public string Software { get; set; }
    }
}
